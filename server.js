const express    = require('express');        // call express
const app        = express();                 // define our app using express
const bodyParser = require('body-parser');

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

let port = process.env.PORT || 5000;        // set our port

const mongoose = require('mongoose');

mongoose.Promise = global.Promise;

mongoose.connect(`mongodb://race34:Raven14!@ds133041.mlab.com:33041/raven_db`, {useNewUrlParser: true })
.then(() => {
    console.log("Successfully connected to database!");
})
.catch( (e) => {
    console.log(e);
    process.exit();
})

require('./app/routes/article.routes.js')(app);
require('./app/routes/auth.routes.js')(app);

app.use((err, req, res, next) => {
    if (err.isBoom) {
        return res.status(err.output.statusCode).json(err.data);
    }
});

app.listen(port);