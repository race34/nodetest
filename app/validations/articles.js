const Joi = require('joi');

const post = {
    newArticle: {
        body: {
            title:  Joi.string().required(),
            slug: Joi.string().required()
        }
    }
};

module.exports = post;