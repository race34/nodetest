const Joi = require('joi');

const user = {
    create: {
        body: {
            username:  Joi.string().required(),
            password: Joi.string().required(),
            email: Joi.string().email()
        }
    },
    login: {
        body: {
            username: Joi.string().required(),
            password: Joi.string().required()
        }
    }
};

module.exports = user;