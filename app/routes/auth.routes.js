module.exports = (app) => {
    const auth = require("../controller/auth.controller.js");
    const expressJoi = require('express-joi-validator');

    const schema = require('../validations/user.js');

    app.post('/users/register', expressJoi(schema.create), auth.create);
    app.get('/users/me', auth.me);
    app.post('/users/login', expressJoi(schema.login), auth.login);
};