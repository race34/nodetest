

module.exports = (app) => {
    const article = require("../controller/article.controller.js");
    const expressJoi = require('express-joi-validator');

    const schema = require('../validations/articles.js')

    app.get('/', article.index);
    app.get('/:article_id', article.read);
    app.post('/', expressJoi(schema.newArticle), article.create);
    app.delete('/:article_id', article.delete);
    app.put('/:article_id',expressJoi(schema.newArticle), article.update);
}