const User = require('../models/user.model.js');
const jwt = require('jsonwebtoken');
const config = require('../../config.js');
const bcrypt = require('bcryptjs');


exports.create = (req, res) => {
    let hashedPassword = bcrypt.hashSync(req.body.password, 8);

    User.findOne({ username: req.body.username}, (err, user) => {
        if (user) {
            res.status(500).json({
                error: "Username Already Exists!"
            });
        } else {
            User.create({
                username: req.body.username,
                password: hashedPassword,
                email: req.body.email
            })
            .then( user => {
                
                let token = jwt.sign({id: user._id}, config.secret, {
                    expiresIn: 86400
                });
        
                res.status(200).json({
                    auth: true,
                    token: token
                });
            })
            .catch( err => {
                res.status(500).json({
                    error: "There was a problem adding User."
                }); 
            });
        }
    });
};

exports.login = (req,res) => {
    User.findOne({ username: req.body.username, password: req.body.password})
    .then( user => {
        let password = bcrypt.compareSync(req.body.password, user.password);

        if (!password) {
            return res.status(401).json({
                auth: false,
                token: null,
                error: "Password is incorrect!"
            });
        }

        const token = jwt.sign({ id: user._id }, config.secret, {
            expiresIn: 86400 //24 hrs
        });

        res.status(200).json({
            auth: true,
            token: token
        });
    })
    .catch(err => {
        res.status(404).json({
            error: "Invalid username/password"
        });

        console.log(err)
    })
}

exports.me = (req, res) => {
    let token = req.headers['x-access-token'];

    if (!token) {
        return res.status(401).json({
            error: "Invalid access token"
        });
    }

    jwt.verify(token, config.secret)
    .then( decoded => {
        User.findById(decoded.id)
        .then( user => {
            res.status(200).json(user);
        })
        .catch( err => {
            res.status(404).json({
                error: "User not found"
            });
        });
    })
    .catch( err => {
        res.status(500).json({
            error: "There was a problem finding the user"
        });
    });
};