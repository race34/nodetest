const Person = require('../models/article.model.js');
const _omit = require('lodash/omit');
const _keys = require('lodash/keys');

exports.index = (req, res) => {
    Person.find({}, (err, persons) => {
        const person = persons.map( data => data);

        res.status(200).json(person)
    });
};

exports.create = (req, res) => {
    const person = new Person(req.body);
    
    person.save()
    .then( data => {
        res.status(200).json(data);
    })
    .catch( err => {
        res.status(500).json({
            message: err.message || "Some Error occurred while creating the Note."
        });
    });
};

exports.read = (req, res) => {
    let slug = {
        slug: req.params.article_id
    };

    let model = {
        _id:null,
        __v:null
    };

    Person.findOne(slug)
    .then( person => {
        res.status(200).json(_omit(person, _keys(model)));
    })
    .catch( err => {
        console.log(err);
        res.status(500).json({
            error: "Can't find the article"
        })
    });
};

exports.delete = (req, res) => {
    Person.findByIdAndRemove(req.params.article_id)
    .then( () => {
        res.status(200).json({
            message: "Successfully Deleted!"
        });
    })
    .catch( err => {
        console.log(err);
        res.status(500).json({
            error: "Not able to delete!"
        });
    });
};

exports.update = (req, res) => {
    Person.findByIdAndUpdate(req.params.article_id, req.body)
    .then( (data) => {
        res.status(200).json(data);
    })
    .catch( err => {
        console.log(err);
        res.status(500).json({
            err: "unable to update"
        });
    });
}