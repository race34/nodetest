const mongoose = require('mongoose');
const Joi = require('joi');
const expessJoi = require('express-joi-validator');

const Schema = mongoose.Schema;

let PostSchema = new Schema({
    title: String,
    body: String,
    slug: String,
    created_date: Date,
    created_by: String
});

module.exports = mongoose.model('Post', PostSchema);

